<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SmkAspekRentangNilai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="smk-aspek-rentang-nilai-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_rentang_nilai')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'predikat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'predikat_nilai')->textInput() ?>

    <?= $form->field($model, 'is_has_rentang')->textInput() ?>

    <?= $form->field($model, 'batas_bawah')->textInput() ?>

    <?= $form->field($model, 'batas_atas')->textInput() ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
