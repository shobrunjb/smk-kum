<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SmkJenisPenilaian */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="smk-jenis-penilaian-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jenis_penilaian')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
